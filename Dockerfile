FROM bellsoft/liberica-openjdk-alpine:11
RUN mkdir /usr/lib/komga &&\
    wget -O /usr/lib/komga/komga.jar https://github.com/gotson/komga/releases/download/v0.165.0/komga-0.165.0.jar
RUN apk add dumb-init
ENV KOMGA_CONFIGDIR="/config"
ENV LC_ALL=en_US.UTF-8
ADD run /usr/local/bin/run
RUN chmod +x /usr/local/bin/run
ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["run"]
VOLUME /config
EXPOSE 8080
